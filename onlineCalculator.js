// Number button click event listener
$(".number").click(function() {
	var num = $(this).val();
	var displayNumber = $("#display").val(); // H timi pou uparxei hdh sto input
	$("#display").val(displayNumber + num);  // String concatenation
});

// Clear button event listener
$("#clearBut").click(function() {
	$("#display").val(""); // clear the display
	sessionStorage.clear();
});

// Equals function
function equals() {
	var displayNumber = $("#display").val();
	if (sessionStorage.getItem("praxi")) { //uparxei apo8hkeumenh praksi etoimh
		var memoryNumber = Number(sessionStorage.getItem("memoryNum"));
		var praxi1 = sessionStorage.getItem("praxi");
		var output;
		if (praxi1 == "+") {
			output = memoryNumber + Number(displayNumber);
		} else if (praxi1 == "-") {
			output = memoryNumber - Number(displayNumber);			
		} else if (praxi1 == "/") {
			output = memoryNumber / Number(displayNumber);
		} else {
			output = memoryNumber * Number(displayNumber);
			
		}
		$("#display").val(output.toString());
		sessionStorage.clear();
	}
};


// Equals button event listener
$("#equalsBut").click(equals);

// Add button event listener
$("#addBut").click(function() {
	if (sessionStorage.getItem("praxi")) {
		equals();
	};
	var displayNumber = $("#display").val();
	sessionStorage.setItem("memoryNum", displayNumber);
	sessionStorage.setItem("praxi", "+");
	$("#display").val("");
});

// Subtract button event listener
$("#subtractBut").click(function() {
	if (sessionStorage.getItem("praxi")) {
		equals();
	};
	var displayNumber = $("#display").val();
	sessionStorage.setItem("memoryNum", displayNumber);
	sessionStorage.setItem("praxi", "-");
	$("#display").val("");
});

// multiply button event listener
$("#multiplyBut").click(function() {
	if (sessionStorage.getItem("praxi")) {
		equals();
	};
	var displayNumber = $("#display").val();
	sessionStorage.setItem("memoryNum", displayNumber);
	sessionStorage.setItem("praxi", "*");
	$("#display").val("");
});

// Divide button event listener
$("#divideBut").click(function() {
	if (sessionStorage.getItem("praxi")) {
		equals();
	};
	var displayNumber = $("#display").val();
	sessionStorage.setItem("memoryNum", displayNumber);
	sessionStorage.setItem("praxi", "/");
	$("#display").val("");
});